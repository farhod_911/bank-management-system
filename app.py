import random

class User:
    def __init__(self, first_name, last_name, age, gender, account_id):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender
        self.account_id = account_id

    def show_details(self):
        print('Account Information')
        print()
        print('Account ID:', self.account_id)
        print('First name:', self.first_name)
        print('Last name:', self.last_name)
        print('Age:', self.age)
        print('Gender:', self.gender)
    

class Bank(User):
    def __init__(self, first_name, last_name, age, gender, account_id):
        super().__init__(first_name, last_name, age, gender, account_id)
        self.balance = 0

    def deposit(self, amount):
        self.balance += amount
        print('Account Balance has been updated: $', self.balance)

    def withdraw(self, amount):
        if amount > self.balance:
            print('Insufficient Funds | Balance available: $', self.balance)
        else:
            self.balance -= amount
            print('Account Balance has been updated: $', self.balance)
        
    def view_balance(self):
        print('Balance available: $', self.balance)


def main_menu():
    print("_"*30)
    print("_"*13+'Menu'+"_"*13)
    print('1. View my balance details')
    print('2. Add Funds to my balance')
    print('3. Withdraw my balance')
    print('4. View my account details')
    print('Q. Exit') 

while True:
    print('_'*30)
    print('1. Create a new bank account')
    print('Q. Exit') 
    print('_'*30)
    command = input(': ').lower()
    if command == '1':
        first_name = input('First Name: ')
        last_name = input('Last Name: ')
        age = input('Age: ')
        gender = input('Gender: ')
        id_number = random.randint(1000, 10000)
        user = User(first_name, last_name, age, gender, id_number)
        bank = Bank(first_name, last_name, age, gender, id_number)
        main_menu()
        print('_'*30)
        run = True
        while run:
            def dashboard():
                global run
                service = input(': ').lower()
                if service != 'q':
                    if service == '1':
                        bank.view_balance()
                        main_menu()
                    elif service == '2':
                        print('Please provide amount of money you want add to your balance')
                        print('_'*30)
                        try:
                            amount = int(input(': '))
                            bank.deposit(amount)
                        except ValueError:
                            print('Invalid Value...')
                        main_menu()
                    elif service == '3':
                        print('How much do you want to withdraw from your balance')
                        print('_'*30)
                        amount = int(input(': '))
                        bank.withdraw(amount)
                        main_menu()
                    elif service == '4':
                        user.show_details()
                    elif service == 'q':
                        run = False
                    else:
                        print("I don't Understand your command...")
                        main_menu()
            dashboard()
    
    elif command == 'q':
        break

    
        


